<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', '123123' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost:3306' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'o$Ll;kIXZ5?lL&rT`y$I0?~uYe?@)>)saIL kt#LcPN:_KW#DiTbg1uy|-pWZgcO' );
define( 'SECURE_AUTH_KEY',  'J:S-_veVP%n*Mqq)G1w,HBe`M///[L&L*KIbQw+<Jz8*Rq4jQ!mhz&&UEC!eC@Ks' );
define( 'LOGGED_IN_KEY',    'Ow?Rq0en)t2&F;3S07%mCPc(*@@fAr>N?Zqi[}|*Pe(Fz&Upkgc#+9RI&}cz-mAl' );
define( 'NONCE_KEY',        'T)D)s8X*XpR9VP!Eu{~w~:3~nV)RLmy^:GI?{gA-FGmC[n#ddi)1ws[`b$34oj)u' );
define( 'AUTH_SALT',        '%jdigA+I;XrZu e7!|L^9=M8?^]$%H;I}%L.k#o[wNFroN%I$M!*sBF<b@I@SOu/' );
define( 'SECURE_AUTH_SALT', '!yr9poR0z}/yDw(nI`7lag^;k#s_AfoW !S1Rr_N!-P,:~EhZZ2aWydy/AyYc6MT' );
define( 'LOGGED_IN_SALT',   'V?e]Q~!W.1=P6QF|3LlltA4t0!BXPO$kbc/fg6< Xt.dgAQg2CS<zE9AubDGJ_Jd' );
define( 'NONCE_SALT',       'F#_w6g Vdf4D9-KW3eb=C>%1W2nAaCnOrFEIL(d&Q0xRe+^l<5,yiMmy+$ zt5P.' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
