<?php

// styles and scripts
function add_theme_scripts() {
    wp_enqueue_style( 'style', get_template_directory_uri() .'/style/main.css');
	  wp_enqueue_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.10.0/jquery.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'script', get_template_directory_uri() .'/js/main.js');
  }
  add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );
  


  /** Form Submittion */
  function send_email() {
    print_r($_POST);
    $firstname = !empty($_POST['firstname']) ? $_POST['firstname'] : false;
    $lastname = !empty($_POST['lastname']) ? $_POST['lastname'] : false;
    $email = !empty($_POST['email']) ? $_POST['email'] : false;
    $company = !empty($_POST['company']) ? $_POST['company'] : false;
    $title = !empty($_POST['title']) ? $_POST['title'] : false;
    $phone = !empty($_POST['phone']) ? $_POST['phone'] : false;
    
    if(!$firstname || !$lastname || !$email || $company || $title || $phone) {
      // invalid parameters
      return new WP_Error(400, "Error - invalid parameters");
    } else {
      echo 'Sending the email';
  }
}

  // non - auth users
  add_action('admin_post_nopriv_send_email', 'send_email');
  // auth users
  add_action('admin_post_send_email','send_email');