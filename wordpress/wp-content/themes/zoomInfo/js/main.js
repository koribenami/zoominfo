jQuery(function ($) {
    $(document).ready(function() {
        //Click on Operations link
        $(".Operations_link").click(function () {
           //display Operations description tab
           $(".Operations.descriptions")[0].style.display = "inline-block";
           //hide Marketing and Sales description tabs
           $(".Marketing.descriptions")[0].style.display = "none";
           $(".Sales.descriptions")[0].style.display = "none";
           //handle Operations images
           $(".Operations_image_after_click")[0].style.display = "inline-block";
           $(".Operations_image")[0].style.display = "none";
           //handle Marketing and Sales images
           $(".Marketing_image_after_click")[0].style.display = "none";
           $(".Marketing_image")[0].style.display = "inline-block";
           $(".Sales_image_after_click")[0].style.display = "none";
           $(".Sales_image")[0].style.display = "inline-block";
           //change background colors 
           $(".operations_tab")[0].style.backgroundColor= "#3f7d95";
           $(".marketing_tab")[0].style.backgroundColor= "#e2dfdf";
           $(".sales_tab")[0].style.backgroundColor= "#e2dfdf";
           //change color 
           $(".operations_tab")[0].style.color= "#ffffff";
           $(".marketing_tab")[0].style.color= "#000000";
           $(".sales_tab")[0].style.color= "#000000";
    
        });
        //Click on Sales link
        $(".Sales_link").click(function () {
           //display Sales description tab
           $(".Sales.descriptions")[0].style.display = "inline-block";
           //hide Marketing and Operations description tabs
           $(".Marketing.descriptions")[0].style.display = "none";
           $(".Operations.descriptions")[0].style.display = "none";
           //handle Sales images
           $(".Sales_image_after_click")[0].style.display = "inline-block";
           $(".Sales_image")[0].style.display = "none";
           //handle Marketing and Operations images
           $(".Marketing_image_after_click")[0].style.display = "none";
           $(".Marketing_image")[0].style.display = "inline-block";
           $(".Operations_image_after_click")[0].style.display = "none";
           $(".Operations_image")[0].style.display = "inline-block";
           //change background colors 
           $(".operations_tab")[0].style.backgroundColor= "#e2dfdf";
           $(".marketing_tab")[0].style.backgroundColor= "#e2dfdf";
           $(".sales_tab")[0].style.backgroundColor= "#3f7d95";
           //change color 
           $(".operations_tab")[0].style.color= "#000000";
           $(".marketing_tab")[0].style.color= "#000000";
           $(".sales_tab")[0].style.color= "#ffffff";
        });
        //Click on Marketing link
        $(".Marketing_link").click(function () {
           //display Marketing description tab
           $(".Marketing.descriptions")[0].style.display = "inline-block";
           //hide Sales and Operations description tabs
           $(".Operations.descriptions")[0].style.display = "none";
           $(".Sales.descriptions")[0].style.display = "none";
           //handle Marketing images
           $(".Marketing_image_after_click")[0].style.display = "inline-block";
           $(".Marketing_image")[0].style.display = "none";
           //handle Sales and Operations images
           $(".Sales_image_after_click")[0].style.display = "none";
           $(".Sales_image")[0].style.display = "inline-block";
           $(".Operations_image_after_click")[0].style.display = "none";
           $(".Operations_image")[0].style.display = "inline-block";
           //change background colors 
           $(".operations_tab")[0].style.backgroundColor= "#e2dfdf";
           $(".marketing_tab")[0].style.backgroundColor= "#3f7d95";
           $(".sales_tab")[0].style.backgroundColor= "#e2dfdf";
           //change color 
           $(".operations_tab")[0].style.color= "#000000";
           $(".marketing_tab")[0].style.color= "#ffffff";
           $(".sales_tab")[0].style.color= "#000000";
        });
    
        $(".freeTrial_pop_up").click(function() {
           $(".freeTrial_popUp")[0].style.display = "flex";
        })

        $(".exit").click(function() {
            $(".freeTrial_popUp")[0].style.display = "none";
         })
     });

     
    })
