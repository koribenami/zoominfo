<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage insurance tamplate
 * @since 1.0.0
 */

do_action('wp_head');
?>
<div class="top_section">
   <div class="top_section_container">
      <h1 class="header"><?php echo get_field("header") ?></h1>
      <span class="sub_header"><?php echo get_field("sub_header") ?></span>
      <a class="top-section_btn freeTrial_pop_up"><?php echo get_field("submit_button") ?></a>
   </div>
</div>

<div class="second_section section_margin">
   <div class="second_section_container">
      <h2 class="title"><?php echo get_field("second_section_title") ?></h2>
      <span class="text"><?php echo get_field("second_section_text") ?></span>
   </div>
</div>


<div class="thired_section section_margin">
   <div class="thired_section_container">
      <h2 class="title"><?php echo get_field("third_section_title") ?></h2>
      <div class="organization_section"> 
         <?php 
          if (have_rows('marketing')) :
            while (have_rows('marketing')) : the_row();
               // loop code
               $marketing_section_title = get_sub_field("marketing_title");
               $marketing_image = get_sub_field("marketing_image");
               $marketing_image_after_click = get_sub_field("marketing_image_after_click");
               echo '
               <div class="'. $marketing_section_title .' descriptions">
                     <h3 class="title">'. $marketing_section_title .'</h3>
                     <ul class="text-list">
                        <li class="text-item"><span>'. get_sub_field("marketing_text1") .'</span></li>
                        <li class="text-item"><span>'. get_sub_field("marketing_text2") .'</span></li>
                     </ul>
                  </div>
               ';
            endwhile;
         endif;
         if (have_rows('operations')) :
         while (have_rows('operations')) : the_row();
            // loop code
            $operations_section_title = get_sub_field("operations_title");
            $operations_image = get_sub_field("operations_image");
            $operations_image_after_click = get_sub_field("operations_image_after_click");
            echo '
            <div class="'. $operations_section_title .' descriptions">
               <h3 class="title">'. $operations_section_title .'</h3>
               <ul class="text-list">
                  <li class="text-item"><span>'. get_sub_field("operations_text1") .'</span></li>
                  <li class="text-item"><span>'. get_sub_field("operations_text2") .'</span></li>
                  <li class="text-item"><span>'. get_sub_field("operations_text3") .'</span></li>
               </ul>
            </div>
            ';
         endwhile;
      endif;
      if (have_rows('sales')) :
      while (have_rows('sales')) : the_row();
         // loop code
         $sales_section_title = get_sub_field("sales_title");
         $sales_image = get_sub_field("sales_image");
         $sales_image_after_click = get_sub_field("sales_image_after_click");
         echo '
            <div class="'. $sales_section_title .' descriptions">
               <h3 class="title">'. $sales_section_title .'</h3>
               <ul class="text-list">
                  <li class="text-item"><span>'. get_sub_field("sales_text1") .'</span></li>
                  <li class="text-item"><span>'. get_sub_field("sales_text2") .'</span></li>
                  <li class="text-item"><span>'. get_sub_field("sales_text3") .'</span></li>
               </ul>
            </div>
            ';
      endwhile;
   endif;
   ?>
         <div class="switching_tabs"> 
            <a class="<?php echo $marketing_section_title ?>_link">
               <div class="marketing_tab">
                     <img src="<?php echo $marketing_image ?>" alt="<?php echo $marketing_section_title ?>" class="<?php echo $marketing_section_title ?>_image"/>
                     <img src="<?php echo $marketing_image_after_click ?>" alt="<?php echo $marketing_section_title ?>" class="<?php echo $marketing_section_title ?>_image_after_click"/>
                     <span class="<?php echo $marketing_section_title ?>_span"><?php echo $marketing_section_title ?></span>
               </div>
            </a>
            <a class="<?php echo $operations_section_title ?>_link">
               <div class="operations_tab">
                  <img src="<?php echo $operations_image ?>" alt="<?php echo $operations_section_title ?>" class="<?php echo $operations_section_title ?>_image"/>
                  <img src="<?php echo $operations_image_after_click ?>" alt="<?php echo $operations_section_title ?>" class="<?php echo $operations_section_title ?>_image_after_click"/>
                  <span class="<?php echo $operations_section_title ?>_span"><?php echo $operations_section_title ?></span>
               </div>
            </a>
            <a class="<?php echo $sales_section_title ?>_link">
               <div class="sales_tab">
                  <img src="<?php echo $sales_image ?>" alt="<?php echo $sales_section_title ?>" class="<?php echo $sales_section_title ?>_image"/>
                  <img src="<?php echo $sales_image_after_click ?>" alt="<?php echo $sales_section_title ?>" class="<?php echo $sales_section_title ?>_image_after_click"/>
                  <span class="<?php echo $sales_section_title ?>_span"><?php echo $sales_section_title ?></span>
               </div>
            </a>
         </div>
      </div>
   </div>
</div>

<div class="video_sectio section_margin">
   <div class="video_sectio_container">
      <h2 class="title"><?php echo get_field("video_title") ?></h2>
      <iframe class="video_iframe" src="<?php echo get_field("video_url") ?>"></iframe>
   </div>
   <div class="data_container">
      <h2 class="title"><?php echo get_field("video_data_title") ?></h2>
      <?php 
      if (have_rows('data_images')) :
      while (have_rows('data_images')) : the_row();
         // loop code
         echo ' 
            <img src="'. get_sub_field("image1").'" alt="image1" class="image1"/>
            <img src="'. get_sub_field("image2").'" alt="image2" class="image2"/>
            <img src="'. get_sub_field("image3").'" alt="image3" class="image3"/>
            <img src="'. get_sub_field("image4").'" alt="image4" class="image4"/>
            <img src="'. get_sub_field("image5").'" alt="image5" class="image5"/>
         ';
         endwhile;
      endif;
         ?>
   </div>
</div>

<div class="final_section"> 
   <div class="final_section_conrtainer">
      <h2 class="title"><?php echo get_field("final_section_title") ?></h2>
      <a class="final-section_btn freeTrial_pop_up"><?php echo get_field("submit_button") ?></a>
   </div>
</div>

<div class="freeTrial_popUp" id="freeTrial_popUp_id">
   <form id="sign_up_form" action="<?php echo admin_url('admin-post.php'); ?>" method="post" onsubmit="" class="signUpForm">
      <div class="exit">X</div>
      <input type="text" name="firstname" id="firstname" placeholder="First Name" required>
      <input type="text" name="lastname" id="lastname" placeholder="Last Name" required>
      <input type="email" name="email" id="email" placeholder="Business Email" required>
      <input type="text" name="company" id="company" placeholder="Company" required>
      <input type="text" name="title" id="title" placeholder="Job Title" required>
      <input type="phone" name="phone" id="phone" placeholder="Direct Phone" required>
      <input type='hidden' name='action' value='send_email' />
      <?php wp_nonce_field('send_email', 'send_email_nonce'); ?>
      <input type="submit" value="Apply Now!">
      <label for="gdpr-textbox" class="label-check">
         <p class="gdpr-checkbox-p">By submitting this form, I agree to ZoomInfo's <a href="https://www.zoominfo.com/about-zoominfo/privacy-center" target="_blank" style="text-decoration: underline;">Privacy Policy</a> and <a href="https://www.zoominfo.com/business/about-zoominfo/terms-conditions" target="_blank" style="text-decoration: underline;">Terms of Service</a>.</p>
         <input id="agreeInputCheckbox" type="checkbox" class="gdpr-textbox" name="gdpr-checkbox" required>
         <span id="agreeSentence" >I agree to receive marketing and other emails from ZoomInfo,</span>
         <span> unless I <a href="https://www.zoominfo.com/unsubscribe" target="_blank" style="color: rgb(63, 125, 148); text-decoration: underline;">opt-out</a>.</span>
      </label>
</form>
</div>
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />